#!/bin/bash

# Token bot Telegram
TELEGRAM_BOT_TOKEN="YOUR_TELEGRAM_BOT_TOKEN"

# ID obrolan atau pengguna Telegram yang akan menerima pesan
TELEGRAM_CHAT_ID_1="YOUR_TELEGRAM_CHAT_ID_1"
TELEGRAM_CHAT_ID_2="YOUR_TELEGRAM_CHAT_ID_2"
TELEGRAM_CHAT_ID_3="YOUR_TELEGRAM_CHAT_ID_3"

# Lokasi file log
LOG_FILE_1="/path/to/your/log/file1.log"
LOG_FILE_2="/path/to/your/log/file2.log"
LOG_FILE_3="/path/to/your/log/file3.log"

# Fungsi untuk mengirim pesan ke Telegram
send_message() {
    chat_id="$1"
    message="$2"
    curl -s -X POST "https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage" -d "chat_id=$chat_id&text=$message"
}

# Simpan hash file log saat ini
previous_hash_1=$(md5sum "$LOG_FILE_1" | awk '{print $1}')
previous_hash_2=$(md5sum "$LOG_FILE_2" | awk '{print $1}')
previous_hash_3=$(md5sum "$LOG_FILE_3" | awk '{print $1}')

while true; do
    # Hitung hash file log saat ini
    current_hash_1=$(md5sum "$LOG_FILE_1" | awk '{print $1}')
    current_hash_2=$(md5sum "$LOG_FILE_2" | awk '{print $1}')
    current_hash_3=$(md5sum "$LOG_FILE_3" | awk '{print $1}')

    # Periksa apakah ada perubahan pada file log 1
    if [ "$current_hash_1" != "$previous_hash_1" ]; then
        log_content_1=$(tail -n 5 "$LOG_FILE_1")
        message_1="$log_content_1"
        send_message "$TELEGRAM_CHAT_ID_1" "$message_1"
        previous_hash_1="$current_hash_1"
    fi

    # Periksa apakah ada perubahan pada file log 2
    if [ "$current_hash_2" != "$previous_hash_2" ]; then
        log_content_2=$(tail -n 5 "$LOG_FILE_2")
        message_2="$log_content_2"
        send_message "$TELEGRAM_CHAT_ID_2" "$message_2"
        previous_hash_2="$current_hash_2"
    fi

    # Periksa apakah ada perubahan pada file log 3
    if [ "$current_hash_3" != "$previous_hash_3" ]; then
        log_content_3=$(tail -n 5 "$LOG_FILE_3")
        message_3="$log_content_3"
        send_message "$TELEGRAM_CHAT_ID_3" "$message_3"
        previous_hash_3="$current_hash_3"
    fi

    # Tunggu selama 60 detik sebelum memeriksa kembali
    sleep 60
done