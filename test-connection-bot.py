import telegram
import asyncio

bot_token = 'BOT-TOKEN'
chat_id = 'ID-CHAT-ROOM'

async def send_telegram_message(message):
    bot = telegram.Bot(token=bot_token)
    await bot.send_message(chat_id=chat_id, text=message)

async def main():
    await send_telegram_message('Testing !!!')
    print('Pesan telah dikirim ke bot Telegram.')

asyncio.run(main())
