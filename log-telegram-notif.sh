#!/bin/bash

TELEGRAM_BOT_TOKEN="BOT-TOKEN"
TELEGRAM_CHAT_ID="ID-CHAT-ROOM"
LOG_FILE="log/test.log"

send_message() {
    message="$1"
    curl -s -X POST "https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage" -d "chat_id=$TELEGRAM_CHAT_ID&text=$message"
}

previous_hash=$(md5sum "$LOG_FILE" | awk '{print $1}')

while true; do
    current_hash=$(md5sum "$LOG_FILE" | awk '{print $1}')

    if [ "$current_hash" != "$previous_hash" ]; then
        log_content=$(tail -n 1 "$LOG_FILE")  # Ubah jumlah baris sesuai kebutuhan
        message="$log_content"
        send_message "$message"

        previous_hash="$current_hash"
    fi

    # sleep 60
done
